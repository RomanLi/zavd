<?php

require_once('config/conf.php');
require_once('model/Db.php');
require_once('mandrill/src/Mandrill.php');

$mandrill = new Mandrill($apiKey);
$db = new Db($dsn, $user, $pass, $opt);
$mails = $db->getMails();
if ($mails) {
    foreach ($mails as $mail) {
        try {
            $message = array(
            'text' => $mail['body'],
            'subject' => $mail['subject'],
            'from_email' => $fromEmail,
            'from_name' => $fromName,
            'to' => array(
                array(
                    'email' => $mail['recipient'],
                    'type' => 'to'
                    )
                ),
            );
            $result = $mandrill->messages->send($message);
            echo 'For mail: ' . $result['0']['email'] . ' status: ' . $result['0']['status'] . "\n";
            $db->setStatus($mail['id'],$result['0']['status']);
        } catch(Mandrill_Error $e) {
            echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
            throw $e;
        }
    }
} else {
    echo 'There is no unsent messages';
}


