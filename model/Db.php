<?php

class Db
{
    protected $dsn;
    protected $user;
    protected $pass;
    protected $opt;
    protected $pdo;

    function __construct($dsn, $user, $pass, $opt)
    {
        $this->dsn = $dsn;
        $this->user = $user;
        $this->pass = $pass;
        $this->opt = $opt;
        $this->pdo = new PDO($this->dsn, $this->user, $this->pass, $this->opt);
    }

    public function getMails()
    {
        $stmt = $this->pdo->prepare("SELECT * FROM mails WHERE status != 'sent' or status IS NULL");
        $stmt->execute();
        return $stmt->fetchAll();
    }
    public function setStatus($id, $status)
    {
        $stmt = $this->pdo->prepare("UPDATE mails SET status = :status WHERE id = :id");
        $stmt->execute(array(':id' => $id, ':status' => $status));
    }
}